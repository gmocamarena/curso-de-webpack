import renderToDOM from './render-to-dom.js';
import makeMessage from './make-messaje.js';

const waitTime = new Promise((todoOk, todoMal) => {
  setTimeout(()=>{
      todoOk('Han pasado 3 segundos, omg');
  }, 3000)
});

module.exports = {
  firtsMessage: 'Hola mundo, desde un modulo',
  delayedMessage: async () => {
    const message = await waitTime;
    console.log(message);
    console.log('ok');
    renderToDOM(makeMessage(message));
  },
}
